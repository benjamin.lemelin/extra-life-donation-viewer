#!/bin/bash
set -e

rm -rf "public"
mkdir "public"
cp "index.html" "public/"
cp "app.css" "public/"
cp "app.js" "public/"